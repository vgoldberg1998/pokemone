﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class PokemonDAL
    {
        static List<Pokemon> pokemones = new List<Pokemon>();

        public void saveList (List<Pokemon> pokes){
         
            pokemones = pokes;
        }
        public void addPokemon(Pokemon p)
        {
            pokemones.Add(p);
        }
        public List<Pokemon> getPokemon()
        {
            return pokemones;
        }
        public List<Pokemon> GetPokemonbyNombre(string nombrePokemon) {
            //me sale diferente al paso del minuto 25.30 del video "Clase 30 mayo parte 2"
            List<Pokemon> nombreString= new List<Pokemon>();
            foreach(Pokemon p in pokemones)
            {
                if(p.name.Equals(nombrePokemon))
                {
                    nombreString.Add(p);
                }
                return nombreString;
            } 
            
        }
    }
}
