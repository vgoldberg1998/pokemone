﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Pokemon
    {
        public string name { get; set; }
        public string url { get; set; }
    }
    public class RespuestaApiPokemon
    {
    public int count { get; set; }
    public object next { get; set; }
    public object previous { get; set; }
    public List<Pokemon> results { get; set; }
}
}
