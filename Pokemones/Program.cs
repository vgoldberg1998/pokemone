﻿using System;
using Newtonsoft;

namespace Pokemones
{
    public partial class Pokedex
    {
        static void Main(string[] args)
        {
            while (getMenu()) ;
            Console.WriteLine("se termino la Pokedex");

        }

        private static bool getMenu()
        {
            bool isMenu= true;
            Console.Clear();
            Console.WriteLine (isMenu); 
            Console.WriteLine ("Menu Pokemon"); 
            Console.WriteLine ("1.- Cargar Pokemon Json"); 
            Console.WriteLine ("2.- Listar Pokemon"); 
            Console.WriteLine ("3.- Duelo"); 
            Console.WriteLine ("0.- Salir");

            String opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    readJsonFile();
                    break;
                case "2":
                    listarPokemon();
                    break;
                case "3":
                    dueloPokemon();
                    break;
                case "0":
                    isMenu = false;
                    break;
                default:
                    Console.WriteLine("opcion invalida");
                    Console.ReadKey();
                    break;

            }

            return isMenu;
        }

       
    }
}
